#!/bin/bash
echo 'We will create an FTP user...'
read -p 'Username: ' USER_USERNAME
read -sp 'Password: ' USER_PASSWORD
echo
read -p 'Home directory: ' USER_HOME_DIR
useradd -d $USER_HOME_DIR -p $(openssl passwd -1 $USER_PASSWORD) $USER_USERNAME
usermod -aG sudo $USER_USERNAME
chown $USER_USERNAME:$USER_USERNAME $USER_HOME_DIR/public_html
echo
echo The user \"$USER_USERNAME\" has been created with home directory \"$USER_HOME_DIR\"

sed -i -e 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd
