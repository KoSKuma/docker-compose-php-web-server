## Installed Packages
- nginx:1.15.8-alpine
- php:7.3.0-fpm-alpine3.8
- mariadb:10.3.11-bionic
- adminer:4.7.0-standalone
- phpmyadmin/phpmyadmin:latest

## Admin
### Database
- user: root
- pass: PASSWORD

## User
### Database
- host: mariadb
- db: DB_NAME
- user: DB_USER
- pass: DB_PASSWORD

### phpMyAdmin
- host: SERVER_IP:8000

### adminer
- host: SERVER_IP:8080

# TODO
- https://codex.wordpress.org/Nginx
